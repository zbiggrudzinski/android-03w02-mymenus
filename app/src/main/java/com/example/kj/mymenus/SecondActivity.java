package com.example.kj.mymenus;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ActionMode;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.example.kj.mymenus.R;

public class SecondActivity extends Activity {

    ActionMode mActionMode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        ActionBar actionBar = this.getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
        TextView tv = (TextView) findViewById(R.id.text_activity_second);
        tv.setOnLongClickListener(new View.OnLongClickListener() {
            public boolean onLongClick(View view) {
                if (mActionMode != null) {
                    return false;
                }

                mActionMode = startActionMode(mActionModeCallback);
                view.setSelected(true);
                return true;
            }
        });
    }

    /*
     * Utworzenie menu opcji
     * Android-03w-interfejs-cz1: slajd 27
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.second, menu);
        return true;
    }

    /*
     * Obsługa menu opcji
     * Android-03w-interfejs-cz1: slajd 27
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.open_main_activity) {
            /*
             * Użycie jawnej intencji do uruchomienia aktywności
             * Android-02w-aplikacje: slajd 31
             */
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.putExtra("ParentClassName", this.getLocalClassName());
            startActivity(intent);
            return true;
        }
        if (id == R.id.open_third_activity) {
            Intent intent = new Intent(getApplicationContext(), ThirdActivity.class);
            intent.putExtra("ParentClassName", this.getLocalClassName());
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Intent getParentActivityIntent() {
        Intent parentIntent = getIntent();
        String className = parentIntent.getStringExtra("ParentClassName");

        Intent newIntent = null;
        try {
            newIntent = new Intent(
                    SecondActivity.this,
                    Class.forName("com.example.kj.mymenus." + className));
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return newIntent;
    }

    /*
     * Odbiornik do obsługa akcji kontekstowych
     * Android-03w-interfejs-cz1: slajdy 29, 30
     */
    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.second_action_mode, menu);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        /*
         * Obsługa akcji kontekstowych
         * Android-03w-interfejs-cz1: slajdy 29, 30
         */
        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {

            switch (item.getItemId()) {
                case R.id.open_main_activity: {
                    /*
                     * Użycie jawnej intencji do uruchomienia aktywności
                     * Android-02w-aplikacje: slajd 31
                     */
                    Intent intent = new Intent(getApplicationContext(),
                            MainActivity.class);
                    startActivity(intent);
                    mode.finish();
                    return true;
                }
                case R.id.open_third_activity: {
                    Intent intent = new Intent(getApplicationContext(),
                            ThirdActivity.class);
                    startActivity(intent);
                    mode.finish();
                    return true;
                }
                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
        }
    };
}
