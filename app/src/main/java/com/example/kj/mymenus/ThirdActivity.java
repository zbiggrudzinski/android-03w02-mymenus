package com.example.kj.mymenus;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.PopupMenu;

import com.example.kj.mymenus.R;

public class ThirdActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_third);
        ActionBar actionBar = this.getActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }
    }

    /*
     * Utworzenie menu opcji
     * Android-03w-interfejs-cz1: slajd 27
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.third, menu);
        return true;
    }

    /*
     * Obsługa menu opcji
     * Android-03w-interfejs-cz1: slajd 27
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.open_main_activity) {
            /*
             * Użycie jawnej intencji do uruchomienia aktywności
             * Android-02w-aplikacje: slajd 31
             */
            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
            intent.putExtra("ParentClassName", this.getLocalClassName());
            startActivity(intent);
            return true;
        }
        if (id == R.id.open_second_activity) {
            Intent intent = new Intent(getApplicationContext(), SecondActivity.class);
            intent.putExtra("ParentClassName", this.getLocalClassName());
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public Intent getParentActivityIntent() {
        Intent parentIntent = getIntent();
        String className = parentIntent.getStringExtra("ParentClassName");

        Intent newIntent = null;
        try {
            newIntent = new Intent(
                    ThirdActivity.this,
                    Class.forName("com.example.kj.mymenus." + className));

        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return newIntent;
    }

    /*
     * Obsługa menu popup
     * Android-03w-interfejs-cz1: slajdy 31, 32
     */
    public void showPopup(View v) {
        PopupMenu popup = new PopupMenu(this, v);
        popup.setOnMenuItemClickListener(new PopupMenu.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.open_main_activity: {
                        /*
                         * Użycie jawnej intencji do uruchomienia aktywności
                         * Android-02w-aplikacje: slajd 31
                         */
                        Intent intent = new Intent(getApplicationContext(),
                                MainActivity.class);
                        startActivity(intent);
                        return true;
                    }
                    case R.id.open_second_activity: {
                        Intent intent = new Intent(getApplicationContext(),
                                SecondActivity.class);
                        startActivity(intent);
                        return true;
                    }
                    default:
                        return false;
                }
            }
        });
        MenuInflater inflater = popup.getMenuInflater();
        inflater.inflate(R.menu.third_popup_menu, popup.getMenu());
        popup.show();
    }

}
