package com.example.kj.mymenus;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.TextView;


public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ActionBar actionBar = this.getActionBar();
        if (actionBar != null) {
            /*
             * Wyłączenie paska tytułu i przycisku Home w pasku akcji
             * Android-03w-interfejs-cz1: slajdy 34
             */
            actionBar.setDisplayShowHomeEnabled(false);
            actionBar.setDisplayShowTitleEnabled(false);
        }
        TextView tv = (TextView) findViewById(R.id.text_activity_main);
        /*
         * zarejstrowanie menu kontekstowego dla widowku tv
         * Android-03w-interfejs-cz1: slajd 28
         */
        registerForContextMenu(tv);
    }

    /*
     * Utworzenie menu opcji
     * Android-03w-interfejs-cz1: slajd 27
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
        return true;
    }

    /*
     * Obsługa menu opcji
     * Android-03w-interfejs-cz1: slajd 27
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.open_second_activity) {
            Intent intent = new Intent(getApplicationContext(), SecondActivity.class);
            intent.putExtra("ParentClassName", this.getLocalClassName());
            startActivity(intent);
            return true;
        }
        if (id == R.id.open_third_activity) {
            Intent intent = new Intent(getApplicationContext(), ThirdActivity.class);
            intent.putExtra("ParentClassName", this.getLocalClassName());
            startActivity(intent);
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    /*
     * Utworzenie menu kontekstowego
     * Android-03w-interfejs-cz1: slajd 28
     */
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main, menu);
    }

    /*
     * Obsługa menu kontekstowego
     * Android-03w-interfejs-cz1: slajd 28
     */
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.open_second_activity:
                /*
                 * Użycie jawnej intencji do uruchomienia aktywności
                 * Android-02w-aplikacje: slajd 31
                 */
                intent = new Intent(getApplicationContext(), SecondActivity.class);
                startActivity(intent);
                return true;
            case R.id.open_third_activity:
                intent = new Intent(getApplicationContext(), ThirdActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onContextItemSelected(item);
        }
    }
}
